print ('Importing modules...')
import numpy
from astropy.io import fits
import pyregion
import pyregion._region_filter as filter
print ('DONE!')

VLASS="VLASS_converted.fits"

img = VLASS

reg_name ='vlass_regions.reg' 
hdulist = fits.open(img)

flxmap = (hdulist[0].data[:,:])
header = (hdulist[0].header)

xs = header["NAXIS1"]
ys = header["NAXIS2"]
size = (ys, xs)		#	this is the size of the image


#	first of all measure the on-source flux
source =  pyregion.open(reg_name)
nsky = len(source) - 1	#	total apertures to measure the sky level

del source[1:]

smask = source.get_mask(shape=size)
flux = 0.0
on_s_pix = 0

for i in range(0,xs-1):
	for n in range(0,ys-1):
		if smask[n,i] == True:
			flux = flux + flxmap[n,i]
			on_s_pix = on_s_pix + 1

#print flux

#	now does the sky measurements
sky_ap = numpy.zeros(nsky)
pix_ap = numpy.zeros(nsky)
sky_pix = []		# This is the array which will contain all the pixels' values in the sky apertures
for k in range(0,nsky):
	sky =  pyregion.open(reg_name)
	del sky[0:k+1]
	del sky[1:nsky]
	bmask = sky.get_mask(shape=size)
	sflx = 0.0
	pix = 0
	for i in range(0,xs-1):
		for n in range(0,ys-1):
			if bmask[n,i] == True:
				sflx = sflx + flxmap[n,i]
				pix = pix + 1  
				
	sky_ap[k] = sflx
	pix_ap[k] = pix
sk_p_pix=sum(sky_ap)/sum(pix_ap)
tflux = flux-sk_p_pix		#	this is the background subtracted flux 

rms=numpy.std(sky_ap)/16  # calculates the rms of the sky, noise
print ('net flux: ',flux, 'Jy')
print ('Background-subtracted flux: ', tflux,' Jy ')
print ('difference: ',(-tflux+flux)/flux*100, '%')
print ('rms= ',rms)
print ('S/N',flux/rms)
print ('DONE')
