# Brian R. Kent, NRAO
# NINE University of West Indies Workshop
# Loading and displaying a FITS image

# IMPORTS ----------------------------------
from astropy.io import fits
from astropy import wcs

import numpy as np
import matplotlib.pyplot as plt

# VARIABLES -------------------------------
RA =   ( 1.0 +11.0/60.0  +09.09/3600.0) * 15.0
DEC = -(13.0 +57.0/60.0 +38.83/3600.0)
HALF_Width = 92   #(92 pixels is approx. 1.5 arcminutes)
VLASS_FITS = 'VLASS1.1.ql.T07t02.J011047-133000.10.2048.v1.I.iter1.image.pbcor.tt0.subim.fits'


#1 open VLASS File. This contains HEADER and DATA
hdulist = fits.open(VLASS_FITS)

#2 Use the VLASS FITS header data to create a world coordinate system(wcs)
w = wcs.WCS(hdulist[0].header)

#3 get all the raw data from the VLASS file
VLASS_image_data = fits.getdata(VLASS_FITS)[0,0,:,:]

#4 Put the coordinates into an Array for AstroPy
world = np.array([[RA, DEC, 0.0, 0.0]], np.float_)

#5 Use the WCS to convert from RA/DEC to pixel locations on the image
Pixel_Coord = w.wcs_world2pix(world,1)
x = int(round(Pixel_Coord[0][1]))
y = int(round(Pixel_Coord[0][0]))

#6 create 4 corners of cutout
lower_x = x - HALF_Width
upper_x = x + HALF_Width

lower_y = y - HALF_Width
upper_y = y + HALF_Width

#7 Use the pixel coordinates to make cutout
vlasscutout = VLASS_image_data[lower_x:upper_x,lower_y:upper_y]

## Here i added these 3 lines of command to create a cuttout of the vlass big image, this contains only the object image centered in its coordinates

obs = hdulist[0].data
obs_head = hdulist[0].header
newheader=obs_head.copy()

newheader[38]=RA
newheader[43]=DEC
#print newheader[38]
#print newheader[43]

outfile='VLASS_cutout.fits'
hdu_s=fits.PrimaryHDU(vlasscutout, header=newheader)
hdu_s.writeto(outfile)



'''
#8 Plot onto graph with Matplotlib
plt.imshow(vlasscutout, origin='lower', cmap='inferno')
plt.colorbar()
plt.show()

'''
