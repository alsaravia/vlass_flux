#!/usr/bin/env python
# -*- coding: utf8 -*-
print ('Importing modules...')
import numpy as np
from astropy.io import fits

print ('Done!')

kk = 2.*np.pi/(360.*3600.)					# one arcsec in rad units

# pacs @ 70

img500 = 'VLASS_cutout.fits'
hdulist500 = fits.open(img500)
header500 = hdulist500[0].header
pixsz500 = np.abs(header500["CDELT1"])*3600
fluxmap=hdulist500[0].data/8.81728274342    #Jy/beam  to Jy/pix


#Now converting from Jy/sr to Jy/px
#flx_reg = fluxmap*kk**2*pixsz500**2

print ('')
print ('Writing the file with the convolved inmage...')
hdu_s = fits.PrimaryHDU(fluxmap,header=header500)
hdu_s.writeto('VLASS_converted.fits')
print ('DONE')
